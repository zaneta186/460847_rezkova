"""
Resenim teto pisemky bude zpracovat 3 dopisy (letter1, letter2 a letter3) od 3 deti, ktere pisou jeziskovi a nakoupit veci z dopisu.
V kazdem dopise je objekt, ktery je mozne koupit na eshopu (promenna stores).
Cilem je nakoupit jeziskovi v eshopech vsechny veci, o ktere si deti napsaly.
Jezisek ma omezeny rozpocet (promenna money), ale muze si vzit uver od banky (promenna bank).
Projdete dopisy deti a najdete v dopisech veci, ktere mate koupit. Veci jsou oddelene - (pomlckou) a jsou vzdy na konci vet.
V dopisech od deti jsou gramaticke chyby, takze bude potreba hledat varianty s i/y nebo ceske prepisy (pro plejstejsn budete hledat playstation).
Tyto veci se pote pokuste najit v eshopech a "koupit". Pri koupeni musite odecist jednu polozku z poctu dostupnych polozek na eshopu a odecist penize jeziska.
Preferujte nejlevnejsi moznost.
Pokud jezisek nebude mit na danou vec dost penez, bude si muset pujcit od banky (odectete penize z banky).
Nakonec programu vytvorite formatovany vypis do konzole, ktery bude obsahovat informace o vsech nakoupenych polozkach (obchod, cena).
Dale bude obsahovat kolik jeziskovy zbylo penez a kolik si pujcil od banky a o kolik vic zaplati na urocich.

SHRNUTI:
1. Zpracujete dopisy od deti (zohlednite gramaticke chyby a ceske tvary)
2. Vyhledate darky z dopisu na eshopu
3. Koupite nejlevnejsi darky na eshopech a odectete pocet polozek v obchode
4. Pokud jeziskovi nevyjdou penize tak si pujcite od banky
5. Vypisete informace o nakupech a jeziskovych financich

ZPUSOB IMPLEMENTACE JE NA VAS. SAMOTNE RESENI (FUNKCE) BUDETE MUSET VYMYSLET SAMI.
"""

letter1 = """Myly jezisku, na vanoce bych si pral -hodinky s vodotriskem"""
letter2 = """Na vanoce bich moc chtel -plejstejsn"""
letter3 = """Na vanoce bych si prala -ponika"""

stores = [
    {'name': 'Alza', 'products': [
        {'name': 'hodinky s vodotryskem', 'price': 150, 'no_items': 5},
        {'name': 'pocitac', 'price': 20000, 'no_items': 2},
        {'name': 'ponik', 'price': 50000, 'no_items': 1}
    ]},
    {'name': 'CZC', 'products': [
        {'name': 'playstation', 'price': 9000, 'no_items': 5},
        {'name': 'Dobroty Ladi Hrusky', 'price': 10, 'no_items': 100},
        {'name': 'ponik', 'price': 70000, 'no_items': 1}
    ]}
]
letter1 = letter1.split(" ")
letter2 = letter2.split(" ")
letter3 = letter3.split(" ")
print(letter1)


# funkce najde a zpracuje darek v dopise (1-2)
def find_gift(letter):
    gift = []
    for word in letter:
        if word[0] == "-":
            if word == "-hodinky":
                gift.append("hodinky")
            elif word == "-plejstejsn":
                gift.append("playstation")
            elif word == "-ponika":
                gift.append("ponik")
    return gift


# print(find_gift(letter3))

def eshop(letter, stores):
    prices_of_gift = []
    gift = find_gift(letter)
    for store_dct in stores:
        for store in store_dct:
            # print(store_dct["products"])
            for gift_lst in store_dct["products"]:
                # print(gift_lst)
                for obj in gift_lst:
                    # print(gift_lst["name"])
                    if gift[0] in gift_lst["name"]:
                        prices_of_gift.append(gift_lst)

    return prices_of_gift


# print(eshop(letter1, stores))


def gift_lst(letter, stores):
    gift = find_gift(letter)
    prices = eshop(letter, stores)
    lst_gift = []
    for dct in prices:
        for _ in dct:
            if gift[0] in dct["name"]:
                lst_gift.append(dct)
    return lst_gift


# print(gift_lst(letter3, stores))

# funkce "koupi" nejlevnejsi darek v obchode podle dopisu
def buy_cheap_gift(letter, stores):
    lst_gift = gift_lst(letter, stores)
    for i in range(len(lst_gift) - 1):
        if lst_gift[i]["price"] < lst_gift[i + 1]["price"]:
            ch = lst_gift[i]["price"]
        else:
            ch = lst_gift[i + 1]["price"]
    return ch, i


# funkce odecte polozku a penize z "money" nebo si jezisek pujci z banky
# (5) funkce vypise info o nakupech
def price_and_items(letter, stores):
    bank = 1000000
    money = 30000
    rate = 19.9

    lst_gift = gift_lst(letter, stores)
    ch = buy_cheap_gift(letter, stores)[0]
    i = buy_cheap_gift(letter, stores)[1]
    for store in stores:
        for product in store["products"]:
            print(stores)
            if lst_gift[i]["price"] == ch:
                lst_gift[i]["no_items"] -= 1
                if money >= ch:
                    money -= ch
                else:
                    bank -= ch
    print("Jeziskovi zbylo", money, sep=" ")
    pujcka = 1000000 - bank
    print("Jezisek si pujcil od banky", pujcka, sep=" ")
    uroky = pujcka * rate
    print("Jezisek zaplati na urocich", uroky, sep=" ")
