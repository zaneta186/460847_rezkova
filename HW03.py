# 1 bod
# z retezce string1 vyberte pouze prvni tri slova a pridejte za ne tri tecky. Vypiste je
# spravny vypis: There are more...
string1 = 'There are more things in Heaven and Earth, Horatio, than are dreamt of in your philosophy.'
list = string1.split(" ")
result = " ".join(list[0:3]) + "..."
print(result)

# 1 bod
# z retezce string1 vyberte pouze poslednich 11 znaku a vypiste je
# spravny vypis: philosophy.
print(string1[-11:-1])


# 1 bod
# retezec string2 rozdelte podle carek a jednotlive casti vypiste
# spravny vypis:
# I wondered if that was how forgiveness budded; not with the fanfare of epiphany
# but with pain gathering its things
# packing up
# and slipping away unannounced in the middle of the night.
string2 = 'I wondered if that was how forgiveness budded; not with the fanfare of epiphany, but with pain gathering its things, packing up, and slipping away unannounced in the middle of the night.'
print("".join(string2.split(",")))
# 2 body
# v retezci string2 spocitejte cetnost jednotlivych pismen a pote vypiste jednotlive cetnosti i pismenem
# spravny vypis: a: 12
#                b: 2
# atd.
string2 = string2.lower()
i = 97
while i <= 122:
    print(chr(i)+":", string2.count(chr(i)))
    i += 1


# 5 bodu
# retezec datetime1 predstavuje datum a cas ve formatu YYYYMMDDHHMMSS
# zparsujte dany retezec (rozdelte ho na rok, mesic, den, hodiny, minuty, sekundy a vytvorte z nej datum tak jak definuje
# funkce datetime. Pouzijete knihovnu a tridu datetime (https://docs.python.org/3/library/datetime.html#datetime.datetime)
# Potom slovy odpovezte na otazku: Co je to cas od epochy (UNIX epoch time)?
# Odpoved:
# Nasledne odectete zparsovany datum a cas od aktualniho casu (casu ziskaneho z pocitace pri zavolani jiste funkce)
# a vypocitejte kolik hodin cini rozdil a ten vypiste
datetime1 = '20181121191930'
import datetime
result_date = datetime.datetime(year=int(datetime1[0:4]), month=int(datetime1[4:6]), day=int(datetime1[6:8]), hour=int(datetime1[8:10]), minute=int(datetime1[10:12]), second=int(datetime1[12:14]))
print(result_date)
today = datetime.datetime.today()
print(today)
print(today - result_date)

"""
Unixový čas je systém pro označení časových okamžiků. Systém identifikuje časové okamžiky 
pomocí počtu sekund uplynulých od okamžiku koordinovaného světového času (UTC) 00:00:00 1. ledna 1970, ale bez započítání přestupných sekund. 
"""

# 5 bodu
# v retezci string3 jsou slova prehazena. V promenne correct_string3 jsou slova spravne.
# vytvorte novou promennou, do ktere poskladate spravne retezec string3 podle retezce correct_string3
# nasledne vypocitej posun mist o ktere muselo byt slovo posunuto, aby bylo dano do spravne pozice.
# nereste pritom smer pohybu
# vypiste spravne poradi vety a jednotliva slova s jejich posunem
# napr. war: 8
#       the: 6
# atd.
string3 = 'war concerned itself with which Ministry The of Peace'
correct_string3 = 'The Ministry of Peace which concerned itself with war'

string3 = string3.split()
correct_string3 = correct_string3.split()
result_string = [" "," "," "," "," "," "," "," "," "]

for i in range(len(string3)):
    for j in range(len(correct_string3)):
        if string3[i] == correct_string3[j]:
            print(correct_string3[j].lower(),":",  abs(i-j))
            result_string[j] = string3[i]

print(" ".join(result_string))


